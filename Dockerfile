#Imagen inicial base/inicial1
FROM node:latest

#Crear directorio de trabajo del contenedor docker
WORKDIR /docker-api-peru

#Copiar archivos del proyecto en el directorio del contenedor
ADD . /docker-api-peru

#Instalar dependencies de produccion
#RUN npm install --only=production No es necesario porque esta in
#ya esta instalado

#Exponer puerto escucha del contenedor (mismo definimos 3002)
EXPOSE 3002

#Lanzar comandos necesarios para ejecutar nuestra API
#CMD ["node","server.js"]
CMD ["npm","run","pro"]
