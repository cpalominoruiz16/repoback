require('dotenv').config();
const express = require('express'); //referencia al paquete expressff
const userFile = require('./user.json');
const bodyParser= require('body-parser');
const requestJSON=require('request-json') ///
const cors = require('cors');
const https = require('https');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"




const baseMLabURL = 'https://api.mlab.com/api/1/databases/techu32db/collections/';
//const apikeyMlab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const apikeyMlab = 'apiKey='+ process.env.MLAB_APIKEY;
const apikeyReniec = 'token='+ process.env.token;
const passCorreo = process.env.pass;
//apiKey='NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF¡'

var app= express(); //paquete express nos va permitir hacer peticiones http
app.use(bodyParser.json()) //para este servidor node app habilita el parseador bodyParser
var port=process.env.PORT || 3002;
const URL_BASE = '/api-peru/v1/';
//whatapp
var request1 = require('request');
var unirest = require("unirest");
app.use(cors());
app.options('*', cors());
const nodemailer = require('nodemailer');
var find= require('find');

//PROYECTO
// GET users a través de mLab
//postman : http://localhost:3002/api-peru/v1/users10
app.get(URL_BASE + 'users10',
  function(req, res) {
    const httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado PORFIN.");
     httpClient.get('usuarios?'  + apikeyMlab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      });
});


//REGISTRAR ACTUALIZADO
//http://localhost:3002/api-peru/v1/usuarios2
/*
{
	"dni" : 66038552,
    "nombres" : "Pedro",
    "apellidos" : "Marmol",
    "celular" : 987654311,
    "email" : "fcaneo2@yolasite.com",
    "requisitos" : true,
    "direccion" : "2 Bowman Place",
    "ocupacion" : "Payment Adjustment Coordinator",
	"cuenta": "CUENTA INDEPENDENCIA",
	"divisa": "SOL"
}
*/
app.post(URL_BASE+'usuarios2',
      function(request,response){
        const httpClient = requestJSON.createClient(baseMLabURL);
        console.log("Cliente HTTP mLab creado para registrar usuario.");
        var val1 = Math.floor(1000 + Math.random() * 9000);
        var val2 = Math.floor(1000 + Math.random() * 9000);
        var val3 = Math.floor(1 + Math.random() * 99);
        var val4 = Math.floor(1 + Math.random() * 9999999999);
        var valCuenta = Math.floor(1 + Math.random() * 987654321);
        console.log("correlativo"+val1);
        var usr = request.body.nombres;
        var newUser = { //objeto
              dni: parseInt(request.body.dni),
              nombres:request.body.nombres,
              apellidos: request.body.apellidos,
              celular: request.body.celular,
              email: request.body.email,
              requisitos: request.body.requisitos,
              direccion: request.body.direccion,
              ocupacion: request.body.ocupacion,
              tarjeta: {
                numTarjeta: "4147-9999-"+val1+"-"+val2,
                tipTarjeta: "SIGNATURE",
                tipParticipante: "Titular",
                pago_atrasado: 0.0,
                pago_minimo_mes: 0.0,
                pago_total_mes: 0.0,
                consumido: 0.0,
                linea_credito: 1200.0
              },
              contacto:{
                        user:usr.substr(0,4)+val3,
                        password:"password"+val3
                      },
              cuentas:
                  [
                       {
                            idCuenta : valCuenta,
                            numCuenta : "0011-0199-"+val4,
                            desc : request.body.desc,
                            saldoContable : 0.0,
                            saldoDisponible : 0.0,
                            divisa : request.body.divisa
                        }
                  ]
                  }
          console.log('datos'+newUser[0]);
          console.log('su nombre es: '+newUser.nombres);
          console.log('su contacto es: '+newUser.contacto.user);
          console.log('su pass es: '+newUser.contacto.password);
          console.log('su cuenta es: '+newUser.cuentas.numCuenta);
          console.log('tarjeta'+newUser.tarjeta.numTarjeta);
          httpClient.post('usuarios?'  + apikeyMlab,newUser,
           function(err, respuestaMLab, body) {
             console.log('estamos en el post');
             console.log('Error: ' + err);
             console.log('Respuesta MLab: ' + respuestaMLab);
             console.log('Body: ' + newUser);
             var res = {};
             if(err) {
                 res = {"msg" : "Error al recuperar users de mLab."}
                 response.status(500);
             } else {
               res = body;
             }
             response.send(res);
              //probamos correoc
              const transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                  user: 'soporte.help.01@gmail.com',
                  pass: passCorreo
                }
              });
              const mailOptions = {
                from: 'soporte.help.01@gmail.com',
                to: request.body.email,
                subject: 'Credenciales de acceso',
                text: 'Se da de alta al cliente. '+'-->Usuario :'+newUser.contacto.user+' '+'-->Password :'+newUser.contacto.password
              };
              transporter.sendMail(mailOptions, function(error, info){
                if (error) {
              	console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                  console.log('Registro satisfactorio!!!');
                }
              });

           });
     });


//buscar un cliente por dni
//http://localhost:3002/api-peru/v1/usuarios/70952151
app.get(URL_BASE+'usuarios/:dni',
      function(request,response){
        console.log("Entra a la operacion get");
        var dni = request.params.dni;
        console.log("capturamos el dni "+dni);
        var queryStringDni = 'q={"dni":'+dni+'}&'
        const httpClient = requestJSON.createClient(baseMLabURL);
        httpClient.get('usuarios?'+ queryStringDni + apikeyMlab,
         function(err, respuestaMLab, body) {
           console.log('entra a get mlab');
           console.log('respu'+respuestaMLab);
           console.log('usuarios?'+ queryStringDni + apikeyMlab);
           var res = {};
          res = body[0];
          var cantCuentas=Object.keys(res.cuentas).length;
          res.cantCuentas=cantCuentas
          console.log(cantCuentas);
           response.send(res);
           console.log("fin");
         }); //httpClient.get(
       });


 // ELIMINAR
 //https://api.mlab.com/api/1/databases/techu32db/collections/usuarios?q={"dni":79426559}&apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF
 //http://localhost:3002/api-peru/v1/usuarios/79426559
 app.delete(URL_BASE+'usuarios/:dni',
       function(request,response){
         console.log("Entra a la operacion delete");
         var dni = request.params.dni;
         console.log("capturamos el dni "+dni);
         var queryStringDni = 'q={"dni":'+dni+'}&'
         const httpClient = requestJSON.createClient(baseMLabURL);
         httpClient.get('usuarios?'+ queryStringDni + apikeyMlab,
          function(err, respuestaMLab, body) {
            console.log('entra a get mlab');
            console.log('respu'+respuestaMLab);
            console.log('usuarios?'+ queryStringDni + apikeyMlab);
            var res = {};
             res = body[0];
            console.log("cuerpo"+JSON.stringify(res._id.$oid));
              httpClient.delete(baseMLabURL+'usuarios/'+res._id.$oid+'?'+ apikeyMlab,
              function(err, respuestaMLab, body) {
                 console.log("Usuario Eliminado");
                 //response.send(body);
                 response.send({"estado": "Cliente eliminado"});
              });
          }); //httpClient.get(
        });

//EDITAR celular
//https://api.mlab.com/api/1/databases/techu32db/collections/usuarios/5ed6c22f490aa60e0db5663f?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF
//POST : http://localhost:3002/api-peru/v1/usuarios/66038552
//BODY: {"celular": 980654493}
app.put(URL_BASE+'usuarios/:dni',
function(request,response){
  console.log("Entra a la operacion editar celular");
  var dni = request.params.dni;
  console.log("capturamos el dni "+dni);
  var queryStringDni = 'q={"dni":'+dni+'}&'
  var celular = request.body.celular;
  var putBody= '{"$set":{"celular":'+celular+'}}'
  console.log("nuevo celular es:"+ celular);
  console.log("putbody es"+ putBody);

  const httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('usuarios?'+ queryStringDni + apikeyMlab,
   function(err, respuestaMLab, body) {
     console.log('entra a get mlab');
     console.log('respu'+respuestaMLab);
     console.log('usuarios?'+ queryStringDni + apikeyMlab);
     var res = {};
     console.log('DDAA '+JSON.stringify(body.length));
     console.log("a ver"+ res);

      res = body[0];
      if(JSON.stringify(body.length)== 0){
        console.log("Usuario no encontrado. No podemos actualizar!");
        response.send("Usuario no encontrado. No podemos actualizar!");
        }else{
          if (!err){
            if (body.length == 1){
              console.log("usuario encontrado");
              httpClient.put('usuarios?'+ queryStringDni + apikeyMlab,JSON.parse(putBody),
              function(errP, respuestaMLabP, bodyP) {
                console.log("Se actualizo celular, validar json");

                var url = 'https://eu44.chat-api.com/instance137189/sendMessage?token=l5ym0yxiq0vpnl2p';
                var data = {
                  phone: '51'+celular,
                  body: 'Se actualizo el numero de celular correctamente. Se envia mensaje por Whatapp para validar la actualizacion...'
                };
                request1({
                  url: url,
                  method: "POST",
                  json: data
                });
                  });
                  response.send({"Actualizacion":"OK"})
                };
            }else{
              console.log("usuario no encontrado. No podemos actualizar!");
            };

          };
          });
        });



 ///LOGIN
 /*
https://api.mlab.com/api/1/databases/techu32db/collections/usuarios?
q={"contacto.user": {$eq: "charlie"},"contacto.password":{$eq: "carlos01"}}&apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF
 */

 /* Hacer LOGIN pasando credenciales en el body */
 //http://localhost:3002/api-peru/v1/login
 /*
 {"user":"charlie",
"password":"carlos01"
}
*/
 app.post(URL_BASE+'login',
   function(request, response) {
     console.log(request.body.user);
     console.log(request.body.password);
     var usuario = request.body.user;
     var password = request.body.password
     var query = 'q={"contacto.user": {$eq: '+JSON.stringify(usuario)+'}'+','+'"contacto.password":{$eq:'+JSON.stringify(password)+'}}'
     var putBody = '{"$set":{"logged":true}}'; /* $ mLab */
     const httpClient = requestJSON.createClient(baseMLabURL);
     //console.log("user?" + query + "&" + mLabAPIkey;

     httpClient.get("usuarios?" + query + "&" + apikeyMlab,
      function(err, resM, body) {
       if (!err) {
         if (body.length == 1) // Login ok
         {
           let res = body;
           console.log("Login successfull!");
           console.log(res[0]);
           var putBody = '{"$set":{"logged":true}}'
           httpClient.put("usuarios?" + query + "&" + apikeyMlab, JSON.parse(putBody),
             function(errP, resP, bodyP) {
               response.send({"login":"ok", "nombre":body[0].nombres,
                         "apellido":body[0].apellidos,
                       "dni":body[0].dni})
           })
         }
         else {
           response.status(404).send('Usuario no encontrado')
         }
       }
     })
   }
 );

 /* Hacer LOGOUT pasando credenciales en el body app.post(URL_BASE+'login',*/
 //http://localhost:3002/api-peru/v1/logout
 /*
 {"user":"charlie",
"password":"carlos01"
}
*/
 app.post(URL_BASE+'logout',
   function(request, response) {
     console.log(request.body.user);
     console.log(request.body.password);
     var usuario = request.body.user;
     var password = request.body.password;
     var query = 'q={"contacto.user": {$eq: '+JSON.stringify(usuario)+'}'+','+'"contacto.password":{$eq:'+JSON.stringify(password)+'}}'
     var putBody = '{"$unset":{"logged":""}}'; /* $ mLab */
     const httpClient = requestJSON.createClient(baseMLabURL);

     httpClient.get("usuarios?" + query + "&" + apikeyMlab,
      function(err, resM, body) {
       if (!err) {
         if (body.length == 1) // Logout ok
         {
           let res = body;
           console.log("Logout successfull!");
           console.log(res[0]);
           var putBody = '{"$unset":{"logged":""}}';
           httpClient.put("usuarios?" + query + "&" + apikeyMlab, JSON.parse(putBody),
             function(errP, resP, bodyP) {
               response.send({"logout":"ok", "nombre":body[0].nombres,
                         "apellido":body[0].apellidos})
           })
         }
         else {
           response.status(404).send('Usuario no encontrado')
         }
       }
     })
   }
 );

//CONSULTA  RENIEC unirest
//http://localhost:3002/api-peru/v1/people/46038552

app.get(URL_BASE + 'people/:dni',
  function (request, response){
    console.log("-->Buscamos en RENIEC<--");
    var dni = request.params.dni;
    var url = 'https://dniruc.apisperu.com/api/v1/dni/'+dni+'?'+apikeyReniec;
    console.log(url);
    request1(url,function (err,response1,body){

      var res = {};
      if(err) {
          res = {"msg" : "Error obteniendo usuario."}
      } else {
        if(body.length > 0) {
          res = body;
          console.log(body);
        } else {
          res = {"msg" : "Usuario no encontrado."}
        }
      }
      response.send(JSON.parse(res));

    });
  });

//buscar movimientos de una cuenta:_ buscar por idCuenta
//http://localhost:3002/api-peru/v1/movimientos/645342876
//https://api.mlab.com/api/1/databases/techu32db/collections/movimientos?q={"idCuenta":481526}&apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF

  app.get(URL_BASE+'movimientos/:idCuenta',
        function(request,response){
          console.log("Entra a la operacion get de la coleccion movimientos");
          var idCuenta = request.params.idCuenta;
          console.log("capturamos el numero de cuenta "+idCuenta);
          var queryStringDni = 'q={"idCuenta":'+idCuenta+'}&'
          const httpClient = requestJSON.createClient(baseMLabURL);
          httpClient.get('movimientos?'+ queryStringDni + apikeyMlab,
           function(err, respuestaMLab, body) {
             console.log('entra a get mlab');
             console.log('respu'+body[0]);
             console.log('movimientos?'+ queryStringDni + apikeyMlab);
             var res = {};
            res = body;
            var salida = {"array":res}
             response.send(salida);
             console.log("fin proceso");
           });
         });

//0011-0057-3616273444
//http://localhost:3002/api-peru/v1/movNumCuenta/0011-0057-3616273444
app.get(URL_BASE+'movNumCuenta/:numCuenta',
      function(request,response){
        console.log("Entra a la operacion get de la coleccion movimientos");
        var numCuenta = request.params.numCuenta;
        console.log("capturamos el numero de cuenta "+numCuenta);
        var queryStringDni = 'q={"numCuenta":'+JSON.stringify(numCuenta)+'}&'
        console.log(queryStringDni);
        const httpClient = requestJSON.createClient(baseMLabURL);
        httpClient.get('movimientos?'+ queryStringDni + apikeyMlab,
         function(err, respuestaMLab, body) {
           console.log('entra a get mlab busqueda de movimientos por numCuenta');
           console.log('respu'+body[0]);
           console.log('movimientos?'+ queryStringDni + apikeyMlab);
           var res = {};
          res = body;
          var salida = {"array":res}
           response.send(salida);
           console.log("fin proceso");
         });
       });


//Registrar movimiento de una cuenta (alta movimientos) pagar tarjeta
 app.post(URL_BASE+'movimientos2',
       function(request,response){
         const httpClient = requestJSON.createClient(baseMLabURL);
         console.log("Cliente HTTP mLab creado para registrar movimientos.");
         var newMov = {
                     idCuenta: request.body.idCuenta,
                     numCuenta: JSON.stringify(request.body.numCuenta),
                     descripcion:JSON.stringify(request.body.descripcion),
                     monto: request.body.monto,
                     fecha: JSON.stringify(request.body.fecha)
                   }
           var queryStringNumCuenta = 'q={"cuentas.numCuenta":'+newMov.numCuenta+'}&';
           var queryStringNumCuentaMov = 'q={"numCuenta":'+newMov.numCuenta+'}&';
           var queryStringNumCuenta2 = 'f={"cuentas.numCuenta":1,"cuentas.idCuenta":1,"cuentas.desc":1,"_id":0}&';

           console.log(queryStringNumCuenta);
           httpClient.get("usuarios?" + queryStringNumCuenta  +queryStringNumCuenta2+ apikeyMlab,
            function(err2, resM2, body2) {
              //console.log(body2[0]);
              var res2={};
              res2=body2[0];
              var i="",x="",numCuentaObt="",descCuenta="";
              var idCuentaObt = 0;
              console.log("SALIO");
              var cant =res2.cuentas.length;
              console.log(res2.cuentas[1].numCuenta);
              console.log(request.body.numCuenta);

              for (i=0; i < cant; i++) {
                if(res2.cuentas[i].numCuenta===request.body.numCuenta){
                  idCuentaObt=res2.cuentas[i].idCuenta;
                  numCuentaObt=res2.cuentas[i].numCuenta;
                  descCuenta = res2.cuentas[i].desc;
                  console.log("ENTRA");
                }else{
                  console.log("Numero de cuenta no encontrada!");
                }
              }

              console.log("resultado");
              console.log(idCuentaObt);
              console.log(numCuentaObt);
              console.log(res2);

           httpClient.get('movimientos?'+ queryStringNumCuentaMov + apikeyMlab,
            function(err1, respuestaMLab1, body1) {
              console.log("estoy en movimientos");
              var res1 = {};
              res1 = body1;
              var numMov=res1.length;
              res3 = body1[numMov-1];
              //obtenemos el ultimo movimiento y sumanos uno para registrar
              if(numMov===0){
              var newIdMovimiento=0;
            }else{
              var newIdMovimiento=res3.idMovimiento;
            }
              var newIdMovimiento2=newIdMovimiento+1;
              console.log(newIdMovimiento2);
              //Json con nuevo movimiento
              var newMov2 = { //objeto
                          idMovimiento:newIdMovimiento2,
                          idCuenta: idCuentaObt,
                          numCuenta: request.body.numCuenta,
                          desc: descCuenta,
                          descripcion:request.body.descripcion,
                          monto: request.body.monto,
                          moneda: request.body.moneda,
                          fecha: request.body.fecha
                        }

           httpClient.post('movimientos?'  + apikeyMlab,newMov2,
            function(err, respuestaMLab, body) {
              console.log('Estamos en el post movimientos');
              console.log('Error: ' + err);
              console.log('Respuesta MLab: ' + respuestaMLab);
              console.log('Body: ' + newMov2);
              var res = {}; //objeto
              if(err) {
                  res = {"msg" : "Error al recuperar users de mLab."}
                  response.status(500);
              } else { //comprobar
                res = body;
              }
              console.log("Registo correcto!!!");
              response.send({"Estado":"Pago satisfactorio","idMovimiento":newMov2.idMovimiento,
                        "idCuenta":newMov2.idCuenta,"numCuenta":newMov2.numCuenta,"desc":newMov2.desc,"descripcion":newMov2.descripcion,"monto":newMov2.monto,
                      "moneda":newMov2.moneda,"fecha":newMov2.fecha});
            });
        });
    });
  });

//servidor escuchara en la url (servidor local)
//con control c para parar el servidor

app.listen(port,function() {
  console.log('NODE js ESCUCHANDO EN EL PUERTO 3002')
}); //funcion anonima
